include help_system.mak
assert-command-present = $(if $(shell which $1),,$(error '$1' missing and needed for this build))
$(call assert-command-present,rst2pdf)
$(call assert-command-present,sed)

.DEFAULT_GOAL = all

CHECKLISTS := $(wildcard lists/*.csv)
STYLEPATH  := style
DOCUMENT_SRCS := $(wildcard *.rst)
DOCUMENT_OBJS := $(addprefix exports/, $(DOCUMENT_SRCS:.rst=.pdf))
ADDON_SRCS := $(wildcard addons/*.rst)
HAVE_RST2PDF := $(shell which rst2pdf)

CHECKLIST_YEAR_SRCS := $(wildcard lists/year_*.csv)

.PHONY: all
all: $(DOCUMENT_OBJS) $(call print-help,all,Build all pdf documents and export them)

exports/checklist_year.pdf: checklist_year.rst style/default_a4.style $(CHECKLIST_YEAR_SRCS)
	$(eval CURRENT_VERSION := $(shell grep '|rev| replace' $< | cut -d'v' -f3))
	$(eval NEXT_VERSION := $(shell echo $(CURRENT_VERSION) + 0.1 | bc ))
	$(shell sed -i -e 's/replace:: v$(CURRENT_VERSION)/replace:: v$(NEXT_VERSION)/' $<)
	./generate_a4.sh ./$<

exports/checklist_work.pdf: checklist_work.rst style/default_a5.style $(CHECKLIST_WORK_SRCS)
	$(eval CURRENT_VERSION := $(shell grep '|rev| replace' $< | cut -d'v' -f3))
	$(eval NEXT_VERSION := $(shell echo $(CURRENT_VERSION) + 0.1 | bc ))
	$(shell sed -i -e 's/replace:: v$(CURRENT_VERSION)/replace:: v$(NEXT_VERSION)/' $<)
	./generate_a5.sh ./$<

.PHONY: commit $(call print-help,commit, Commit all current changes.)
done: export _check = $(call assert-command-present,git)
done:
	#git add .
	#git commit -am 'Update Checklists'
	#git push origin master
