#!/usr/bin/env sh
# Script to generate a checklist.
#
# $1 = Filename of the checklist to renger.
if [ -z "$1" ]; then
  echo "Pleave provide rst file as parameter. Abort."
  exit 1
fi

test ! -d exports && mkdir exports

rst2pdf \
  --stylesheet-path=style \
  -s default_a4.style \
  -e preprocess \
  -c \
  --use-floating-images \
  --disable-splittables \
  -o "exports/${1}.pdf" \
  "${1}"

