.. Table with time effiency calculation per six months and the original from xkcd
.. raw:: pdf

     PageBreak oneColumn

.. .. image:: img/is_it_worth_the_time_2x.png
..    :width: 1500

Add-on: Overview about time vs. effort.

.. class:: table-row-column

   .. list-table:: How long can you work on making a routine task more efficient before you're spending more time than you save? (across six months) `Source <https://xkcd.com/1205/>`__
      :header-rows: 1
      :stub-columns: 1
      :widths: 23 14 13 13 17 18

      * - TIME TO SPEND -> /HOW OFTEN
        - 50/DAY
        - 5/DAY
        - DAILY
        - WEEKLY
        - MONTHLY
      * - 1 SEC
        - 48 MIN
        - 12 MIN
        - 3 MIN
        - 24 SEC
        - 6 SEC
      * - 5 SEC
        - 4 H
        - 1 H
        - 15 MIN
        - 2 MIN
        - 30 SEC
      * - 30 SEC
        - 3 D
        - 6 H
        - 90 MIN
        - 12 MIN
        - 3 MIN
      * - 1 MIN
        - 6 D
        - 36 H
        - 3 H
        - 24 MIN
        - 6 MIN
      * - 5 MIN
        - 7.5 D
        - 1.5 W
        - 15 H
        - 2 H
        - 30 MIN
      * - 30 MIN
        - 45 D
        - 9 W
        - 90 H
        - 36 H
        - 3 H
      * - 1 H
        -
        - 18 W
        - 4.5 W
        - 2 DAYS
        - 6 H
      * - 6 H
        -
        -
        - 27 W
        - 12 D
        - 36 H
      * - 1 DAY
        -
        -
        -
        - 15 D
        - 45 H

.. code:: bash

   * D: Day(s)
   * H: Hour
   * W: week
   * MIN: Minutes
   * SEC: Seconds
