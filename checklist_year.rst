.. |title| replace:: Year Checklist
.. |rev| replace:: v9.2
.. |date| date:: %Y-%m-%d

.. header::

   .. class:: header-table

   +--------------+---------------------+--------------------------------+
   |              | .. class:: h1center | .. class:: right               |
   |              |                     |                                |
   |              |    |title|          |    |rev| / |date|              |
   |              |                     |                                |
   |              |                     |    PAGE ###Page###/###Total### |
   +--------------+---------------------+--------------------------------+

.. class:: table-checklist
.. csv-table:: January
   :file: lists/year_january.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: February
   :file: lists/year_february.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: March
   :file: lists/year_march.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: April
   :file: lists/year_april.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: May
   :file: lists/year_may.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: June
   :file: lists/year_june.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: July
   :file: lists/year_july.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: August
   :file: lists/year_august.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: September
   :file: lists/year_september.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: October
   :file: lists/year_october.csv
   :widths: 70 30

.. page::

.. class:: table-checklist
.. csv-table:: November
   :file: lists/year_november.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: December
   :file: lists/year_december.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: WINTER PREPARATION
   :file: lists/year_winter.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: SUMMER PREPARATION
   :file: lists/year_summer.csv
   :widths: 70 30

.. class:: table-checklist
.. csv-table:: Unscheduled
   :file: lists/year_unscheduled.csv
   :widths: 70 30
