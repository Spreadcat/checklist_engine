.. |title| replace:: Work Checklist
.. |rev| replace:: v3.7
.. |date| date:: %Y-%m-%d
.. |logo| image:: img/company_logo.png
   :scale: 80

.. header::

   .. class:: header-table

   +--------------+---------------------+--------------------------------+
   |              | .. class:: h1center | .. class:: right               |
   |              |                     |                                |
   |              |    |title|          |    |rev|                       |
   |              |                     |    |date|                      |
   |              |                     |    PAGE ###Page###/###Total### |
   +--------------+---------------------+--------------------------------+

|logo|

.. `MONTHLY`
.. class:: table-checklist
.. csv-table:: Monthly
   :file: lists/work_monthly.csv
   :widths: 86 14

.. class:: table-checklist
.. csv-table:: January
   :file: lists/work_january.csv
   :widths: 86 14

.. class:: table-checklist
.. csv-table:: April
   :file: lists/work_april.csv
   :widths: 86 14

.. include:: addons/table-task_time_effiency.rst
