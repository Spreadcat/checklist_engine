# Checklist engine

This project contains a simple engine to create even simpler checklists from restructuredText documents.

Use the two example checklist in the root directory as a starting point to either modify or to build further.

[[_TOC_]]

## TL;DR

1. Update a list in the sub-directory `./lists`.
1. Run `make` in order to generate a new PDF document for a list in `./exports`.

## Requirements

### rst2pdf

This project requires [`rst2pdf`][#rst2pdf] to be installed.

### Podman/Docker

If you want to run the project in a container and generate the lists from there.
The example below uses `podman`, use docker the same way.

```bash
podman
podman run \
  --rm \
  --volume $(pwd):/rst2pdf:z \
  --entrypoint=/rst2pdf/generate.sh \
  docker.io/opdavies/rst2pdf \
  checklist_year.rst
```

### Fedora 38

```bash
sudo dnf install -y \
  make \
  rst2pdf
```

### Ubuntu 20.04/22.04

```bash
sudo apt-get install -y \
  make \
  rst2pdf
```

## Files and directories

### General overview

These are the files and directories in the root directory of the project.

File | Description
--- | ---
addons | Directory for non-list documents for rst2pdf.
checklist_year.rst | Example checklist with two column layout.
docs | Some project related files.
exports | Export directory for the PDF documents.
generate_a4.sh | Wrapper script for rst2pdf to generate checklists with a default style-sheet in A4.
generate_a5.sh | Wrapper script for rst2pdf to generate checklist with A5 paper format.
help_system.mak | Helper file for `make`.
img | Directory to contain image files for checklists.
lists | Directory containing the CSV lists with the checklist data.
Makefile | File for the tool `make` to generate checklists.
README.md | Readme file for the whole project.
style | style directory containing the rst2pdf style definitions.

### Checklists

The following files and directories are relevant for the checklists.

File | Description
--- | ---
addons/*.rst | Directory which may contain additional rst files included in `checklist_year.rst`.
checklist_year.rst | Main file to handle the included lists and structure of the checklist.
img/* | Directory with images that may be contained in the checklist.
lists/year_*.csv | Directory with CSV files that contain checklists.

### Document rendering

File | Description
--- | ---
exports/ | Directory where PDF documents are stored in.
generate_a4.sh/ | Wrapper script for rendering the example Checklist in A4.
generate_a5.sh/ | Wrapper script for rendering the example Checklist in A5.
help_system.mak | Help file for `make`.
Makefile | Configuration file for `make` to generate the checklist PDF document.
style/ | Directory with document styles that are included during the document rendering.

## How it works

The central document for a checklist is the restructuredText file in the root-directory. From there all data is imported
from CSV files in the `lists/` sub-directory.

New lists require another restructuredText file with the corresponding imports.

### checklist_year.rst

|   |   |
| --- | --- |
| ![Checklist year](./img/overview_checklist_year.png "Checklist year"){width="200px"} | A checklist layout for structuring a year. The layout uses two columns on a A4 paper to display the information. |

### checklist_work.rst

|   |   |
| --- | --- |
| ![Checklist_work](./img/overview_checklist_work.png "Checklist work"){width="200px"} | A layout for a list i use at work. A company logo makes clear what this list is about, the format is A5, single column. |

### Common usage

The document begins with defining the title, review number and date of when the document is being generated and
assigning it to the corresponding variables.

```rst
.. |title| replace:: Year Checklist
.. |ref| replace:: v1.0
.. |date| date:: %Y%-%m-%d
```

These variables are then re-used in the head section inside the directive `header-table`.
The variable `ref` is also automatically increased by the `Makefile` when rendering the PDF documents with that.

The class directive `csv-table` imports a csv file in any given sub-directory. In the example file
`checlist_year.csv` a file `lists/year_january.csv` is imported. RestructuredText imports the content and creates a
table with the dimensions defined in the directive:

```rst
.. csv-table:: January
   :file: lists/year_january.csv
   :widths: 70 30
```

The directive `table-checklist` assigned the layout define in styles to the table.

```rst
.. class:: table-checklist
```

### lists/*.csv

The CSV files being used for import do not contain anything special. According to the number of rows defined in the
`csv-table` directive in `checklist_year.rst` the data is imported and spread out over the table.

```csv
"Task1","CHK"
"Task2","CHK"
```

### Styles

Information about how to use styles in `rst2pdf` can be found in the rst2pdf [manual][#rst2pdf_manual].

#### style/default_a4.style

This style is used by `checklist_year.rst` and creates a two-column layout on an A4 page.

Most of the settings are default to `rst2pdf`, but the style definition has been extended with the class
`table-checklist` which re-defines the checklist table layout.

#### style/default_a5.style

This style is used by `checklist_work.rst` and creates a single column checklist on an A5 page. Since the page width is
smaller than A4, a single column makes more sense.

## License

[Gnu GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Contribution

If you'd like to contribute, please do not hesitate to open issues, fork or involved you in any way.

[#rst2pdf]: https://rst2pdf.org/
[#rst2pdf_manual]: https://rst2pdf.org/static/manual.html#styles-in-detail
